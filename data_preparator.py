import csv
import cv2
import glob
import numpy as np
import os, shutil
import dsv
import pandas as pd
import time

classes_counts_in_training = {}
truth_for_training_data = {}

def init_classes_counts(class_counts):
	nr_of_classes = class_counts.shape[0]
	index = 0
	while index < nr_of_classes:
		classes_counts_in_training[str(class_counts.iloc[index].name)] = 0
		index += 1


def prepare_data_folder(folder, new_dir_name):
	path = folder + "/" + new_dir_name
	if not os.path.exists(path):
		os.makedirs(path)
	else:
		clear_folder(path)


def clear_folder(path):
	files = glob.glob(path + "/*")
	for f in files:
		os.remove(f)


def add_to_training_data(sample_name, sample_class, folder):
	path = folder + "/training_data"
	image = cv2.cvtColor(cv2.imread(folder + "/" + sample_name), cv2.COLOR_BGR2GRAY)
	cv2.imwrite(os.path.join(path , sample_name), image)
	truth_for_training_data[sample_name] = sample_class


def add_to_test_data(sample_name, sample_class, folder):
	path = folder + "/test_data"
	image = cv2.cvtColor(cv2.imread(folder + "/" + sample_name), cv2.COLOR_BGR2GRAY)
	cv2.imwrite(os.path.join(path , sample_name), image)


def create_training_testing_data(sample_name, sample_class, class_counts, trainig_data_amount, folder):
		current_count = classes_counts_in_training[str(sample_class)]
		total_class_samples = class_counts.loc[sample_class][0]

		if current_count < total_class_samples * trainig_data_amount:
			add_to_training_data(sample_name, sample_class, folder)
			classes_counts_in_training[str(sample_class)] = current_count + 1
		else:
			add_to_test_data(sample_name, sample_class, folder)


def main():
	folder = "/Users/petrnovota/Desktop/ETH/Kybernetika_a_umela_inteligence/cviceni/klasifikatory.nosync/train_700_28"
	data = pd.read_csv(folder + "/truth.dsv", sep=":", header=None)
	training_data_amount = 0.9

	class_counts = data.groupby(1).count()
	init_classes_counts(class_counts)
	print(type(class_counts.iloc[1].name))
	print(classes_counts_in_training)

	# randomize the order of data
	random_data = data.iloc[np.random.permutation(len(data))]
	random_data.reset_index(drop=True)

	# create test and training data folders or clear existing ones
	prepare_data_folder(folder, "test_data")
	prepare_data_folder(folder, "training_data")
	time.sleep(2)


	# divide data into training and test datasets, we want 80% data in training data set and the rest in testing dataset
	[create_training_testing_data(x, y, class_counts, training_data_amount, folder) for x, y in zip(random_data[0], random_data[1])]

	# save truth table into training data folder
	with open(folder + "/training_data/truth.dsv", mode='w') as truth_file:
		employee_writer = csv.writer(truth_file, delimiter=':')
		for key, val in truth_for_training_data.items():
			employee_writer.writerow([key, val])





if __name__ == "__main__":
	main()