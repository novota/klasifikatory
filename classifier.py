import csv
import os
import sys

import matplotlib.image as mpimg
import numpy as np

class_to_index_mapper = {}
class_counts = {}


def load_images_from_folder(fldr):
	imgs = []
	for filename in os.listdir(fldr):
		if filename.endswith(".png"):
			img = mpimg.imread(os.path.join(fldr, filename))
			if img is not None:
				img = img * 255
				img = img.flatten()
				img = img.astype(int)
				imgs.append((img, filename))
	return imgs


def load_truth_dsv(training_data_folder):
	truth = {}
	with open(training_data_folder + "/truth.dsv") as csv_file:
		csv_reader = csv.reader(csv_file, delimiter=':')
		for row in csv_reader:
			truth[row[0]] = str(row[1])
	return truth


def init_probabilities_table(image_size, nr_of_color_tones, nr_of_classes, smoothen=0):
	result = np.zeros((image_size, nr_of_color_tones, nr_of_classes))
	result += smoothen
	return result


def get_unique_values_from_dic(true_classes):
	result = []
	for key, value in true_classes.items():
		if result.count(value) == 0:
			result.append(value)
	return result


def map_classes_to_indexes(true_classes):
	unique_values_in_dic = get_unique_values_from_dic(true_classes)
	unique_values_in_dic.sort()
	for index, element in enumerate(unique_values_in_dic):
		class_to_index_mapper[str(element)] = index


def get_apriori_probability(nr_of_samples):
	result = []
	for key, value in class_to_index_mapper.items():
		result.append(class_counts[key] / nr_of_samples)
	return np.array(result)


def run_naive_bayes_classificator_training(training_images, test_images, true_classes: dict, nr_of_color_tones):
	image_size = training_images[0][0].shape[0]
	nr_of_classes = len(get_unique_values_from_dic(true_classes))
	probabilities_table = init_probabilities_table(image_size, nr_of_color_tones, nr_of_classes, smoothen=1)

	# count pixel values for each class
	for index, training_image in enumerate(training_images):
		image_name = training_image[1]
		image_class = class_to_index_mapper[true_classes[image_name]]
		for idx, value in enumerate(training_image[0]):
			probabilities_table[idx, value, image_class] += 1

	# convert to probabilities
	soucet = probabilities_table.sum(axis=2)
	sum_extended = np.zeros((image_size, nr_of_color_tones, nr_of_classes))
	for i1, pixel in enumerate(sum_extended):
		for i2, color in enumerate(pixel):
			for i3, cls_nr in enumerate(color):
				sum_extended[i1, i2, i3] = soucet[i1, i2]

	probabilities_table[:, :, :] = probabilities_table[:, :, :] / sum_extended

	# test classificator on test images
	classification = {}
	apriori_probability = get_apriori_probability(len(training_images))
	for index, test_image in enumerate(test_images):
		likelyhood_table = np.zeros(nr_of_classes, dtype=np.float128)
		first = True
		image_name = test_image[1]
		for idx, value in enumerate(test_image[0]):
			if first:
				likelyhood_table += probabilities_table[idx, value, :]
				first = False
			else:

				likelyhood_table *= probabilities_table[idx, value, :]
		# print(likelyhood_table)
		total_probabilities = likelyhood_table * apriori_probability
		best_idx = np.where(total_probabilities == np.amax(total_probabilities))[0][0]
		best_class = None
		for key, value in class_to_index_mapper.items():
			if value == best_idx:
				best_class = key
		classification[image_name] = best_class

	return classification


def count_class_occurencies(true_classes):
	for key, value in true_classes.items():
		if value in class_counts:
			class_counts[value] = class_counts[value] + 1
		else:
			class_counts[value] = 1


def save_classification(bayes_classification, folder, output_name):
	with open(output_name, mode='w') as truth_file:
		employee_writer = csv.writer(truth_file, delimiter=':')
		for key, val in bayes_classification.items():
			employee_writer.writerow([key, val])


def evaluate_result(bayes_classification, true_classes):
	correct = 0
	false = 0
	for key, value in bayes_classification.items():
		if true_classes[key] == value:
			correct += 1
		else:
			false += 1
	print(f"correct predictions {correct} and wrong predictions {false}")


def most_common_class(diff_results) -> str:
	classes = []
	for result in diff_results:
		classes.append(result[1])
	most_common = ("", 0)
	for clas in classes:
		if classes.count(clas) > most_common[1]:
			most_common = (clas, classes.count(clas))
	return most_common[0]


def run_k_classifier(training_images, test_images, true_classes, knn):
	classification = {}
	for test_image in test_images:
		diff_results = []
		for training_image in training_images:
			diff = test_image[0] - training_image[0]
			diff = diff ** 2
			diff = np.sqrt(np.sum(diff))

			if len(diff_results) <= knn:
				diff_results.append((diff, true_classes[training_image[1]]))
			else:
				result = (diff, true_classes[training_image[1]])
				worst_guess = max(diff_results)
				if result < worst_guess:
					diff_results.remove(worst_guess)
					diff_results.append(result)

		# choose the most common class
		classification[test_image[1]] = most_common_class(diff_results)

	return classification


def main(training_data_folder, test_data_folder, output_name, knn, which_classifier):
	training_images = load_images_from_folder(training_data_folder)

	# all_classes = load_truth_dsv("/Users/petrnovota/Desktop/ETH/Kybernetika_a_umela_inteligence/cviceni/klasifikatory/train_1000_28")

	true_classes = load_truth_dsv(training_data_folder)
	count_class_occurencies(true_classes)

	test_images = load_images_from_folder(test_data_folder)

	# images are saved as [(image as np array, image name)]

	map_classes_to_indexes(true_classes)

	nr_of_color_tones = 256

	if which_classifier == "-b":
		bayes_classification = run_naive_bayes_classificator_training(training_images, test_images, true_classes, nr_of_color_tones)
	else:
		bayes_classification = run_k_classifier(training_images, test_images, true_classes, knn)
	# evaluate_result(bayes_classification, all_classes)
	save_classification(bayes_classification, test_data_folder, output_name)


if __name__ == "__main__":
	args = sys.argv
	classifier = args[1]
	if classifier != "-b":
		k = int(args[2])
		output = args[4]
		training_data = args[5]
		test_data = args[6]
	else:
		k = 0
		output = args[3]
		training_data = args[4]
		test_data = args[5]

	# manual testing inputs

	# training_data = "/Users/petrnovota/Desktop/ETH/Kybernetika_a_umela_inteligence/cviceni/klasifikatory/train_1000_28/training_data"
	# test_data = "/Users/petrnovota/Desktop/ETH/Kybernetika_a_umela_inteligence/cviceni/klasifikatory/train_1000_28/test_data"
	# output = "/Users/petrnovota/Desktop/ETH/Kybernetika_a_umela_inteligence/cviceni/klasifikatory/train_1000_28/test_data/classifier.dsv"
	# classifier = "-k"
	# k = 1

	main(training_data, test_data, output, k, classifier)
